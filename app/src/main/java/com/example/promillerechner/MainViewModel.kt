package com.example.promillerechner

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.promillerechner.data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val userDao: UserDao = CustomDatabase.getDatabase(application).userDao()
    private val drinkDao: DrinkDao = CustomDatabase.getDatabase(application).drinkDao()
    private val calculationDao: CalculationDao =
        CustomDatabase.getDatabase(application).calculationDao()

    fun addUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            userDao.addUser(user)
        }
    }

    fun getUsers(): List<User> = runBlocking {
        userDao.getUsers()
    }

    fun getSelectedUser(): User = runBlocking {
        userDao.getSelectedUser()
    }

    fun updateUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            userDao.updateUser(user)
        }
    }

    fun deleteUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            userDao.deleteUser(user)
            calculationDao.deleteEntriesOfUser(user.id)
        }
    }


    fun addDrink(drink: Drink): Long = runBlocking {
        drinkDao.addDrink(drink)
    }

    fun getDrinkIdFromRowId(rowId: Long): Int = runBlocking {
        drinkDao.getDrinkIdFromRowId(rowId)
    }

    fun getAlphabetizedDrinks(): List<Drink> = runBlocking {
        drinkDao.getAlphabetizedDrinks()
    }

    fun getDrinkById(id: Int): Drink = runBlocking {
        drinkDao.getDrinkById(id)
    }

    fun removeDrink(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            drinkDao.deleteDrinkById(id)
        }
    }


    fun getEntriesByDayAndUser(date: Int, userId: Int): List<CalculationEntry> = runBlocking {
        return@runBlocking calculationDao.getEntriesByDayAndUser(date, userId)
    }

    fun addEntry(entry: CalculationEntry) = runBlocking {
        calculationDao.addEntry(entry)
    }

    fun deleteEntryById(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            println("Id: $id")
            calculationDao.deleteEntryById(id)
        }
    }
}