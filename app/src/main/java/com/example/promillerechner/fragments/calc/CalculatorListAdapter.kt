package com.example.promillerechner.fragments.calc

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.CalculationEntry


class CalculatorListAdapter(
    val entries: ArrayList<CalculationEntry>,
    private val viewModel: MainViewModel,
    private val adapterCallback: AdapterCallback
) :
    RecyclerView.Adapter<CalculatorListAdapter.ViewHolder>() {

    interface AdapterCallback {
        fun deleteEntry()
    }

    // Replace all data in the RecyclerView with the new Array
    // Attention! The new data has to be sorted!
    fun repopulateData(newData: ArrayList<CalculationEntry>) {
        entries.clear()
        entries.addAll(newData)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.calcNameTextView)
        val alcvolTextView: TextView = view.findViewById(R.id.calcAlcVolTextView)
        val amountTextView: TextView = view.findViewById(R.id.calcAmountTextView)
        val drinkTimeTextView: TextView = view.findViewById(R.id.calcDrinkTimeTextView)
        val deleteButton: Button = view.findViewById(R.id.calcDeleteImageButton)
    }

    // Function, that creates new ViewHolders as needes by the view
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // each ViewHolder gets the generic list item layout, but no specific data
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.calculator_list_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Function, that binds a ViewHolder to a specific data element
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Fill the empty item layouts with actual data
        viewHolder.nameTextView.text = viewModel.getDrinkById(entries[position].drinkId).name
        viewHolder.alcvolTextView.text =
            (viewModel.getDrinkById(entries[position].drinkId).alcvol.toFloat() / 10).toString() + "%"
        viewHolder.amountTextView.text = entries[position].amount.toString() + "ml"
        viewHolder.drinkTimeTextView.text = (entries[position].drinkTime / 100).toString()
            .padStart(2, '0') + ":" + (entries[position].drinkTime % 100).toString()
            .padStart(2, '0') + " Uhr"

        // If remove-button of entry is clicked, remove it from dataset and notify the adapter
        viewHolder.deleteButton.setOnClickListener {
            // Remove from database
            println("Position: $position, Size: " + entries.size)
            println("Entry: " + entries[position])
            viewModel.deleteEntryById(entries[position].id)
            // Remove from recyclerview
            entries.removeAt(position)
            // Notify adapter about change
//            notifyItemRemoved(position)
//            notifyItemRangeChanged(position, itemCount - position)
            notifyDataSetChanged()

            // Callback to Fragment to repaint the graph
            adapterCallback.deleteEntry()
        }
    }

    // Function, that returns the dataset's size
    override fun getItemCount() = entries.size
}