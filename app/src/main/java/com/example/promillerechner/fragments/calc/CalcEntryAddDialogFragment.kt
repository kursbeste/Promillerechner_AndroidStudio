package com.example.promillerechner.fragments.calc

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AutoCompleteTextView
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProvider
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class CalcEntryAddDialogFragment : DialogFragment() {
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calc_add_dialog, container, false)
    }

    // Configure the Dialog to fill the whole width of the screen (with some margin)
    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        // Instantiate the addButton and disable it
        val addBtn = view.findViewById<MaterialButton>(R.id.addCalcEntryBtnAccept)
        addBtn.isEnabled = false

        // Instantiate the needed input fields and layouts
        val dropdown: AutoCompleteTextView = view.findViewById(R.id.chooseDrinkAutoCompleteTextView)
        val amountEditText = view.findViewById<TextInputEditText>(R.id.addCalcEntryAmountInputField)
        val dropDownLayout: TextInputLayout = view.findViewById(R.id.textInputLayoutDrinks)
        val timePicker = view.findViewById<TimePicker>(R.id.drinkTimePicker)
        timePicker.setIs24HourView(true)

        // Load all drinks from database
        val drinksList = viewModel.getAlphabetizedDrinks()
        // Only need the names for the dropdown: create a new array
        val drinkNames: Array<String?> = arrayOfNulls(drinksList.size)
        for (i in drinksList.indices) {
            drinkNames[i] = drinksList[i].name
        }

        // Create a custom ArrayAdapter that lists all drink names and the alcvol
        val arrayAdapter = CalcAddDialogDropdownAdapter(requireContext(), drinksList)
        // Assign the adapter to the dropdown
        dropdown.setAdapter(arrayAdapter)

        // Error Handling: Add a changeListener to the dropdown to display an error message and
        //      deactivate the button, if input is invalid
        dropdown.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (drinkNames.isEmpty() || drinkNames.indexOf(dropdown.text.toString()) == -1) {
                    dropDownLayout.error = getString(R.string.invalid)
                    addBtn.isEnabled = false
                } else {
                    dropDownLayout.error = null
                    addBtn.isEnabled = amountEditText.text.toString().trim().isNotEmpty()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


        // Error Handling: Add a changeListener to the amount input to deactivate the button,
        //      if input is invalid
        amountEditText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                addBtn.isEnabled = amountEditText.text.toString().trim().isNotEmpty()
                        && drinkNames.isNotEmpty()
                        && (drinkNames.indexOf(dropdown.text.toString()) != -1)
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        // Add clickListener to the add-button to transmit the newly added entry to host fragment
        addBtn.setOnClickListener {
            var drinkId: Int = -1
            for (i in drinksList.indices) {
                if (drinksList[i].name == dropdown.text.toString()) {
                    drinkId = drinksList[i].id
                }
            }

            val drinkTime = timePicker.hour * 100 + timePicker.minute

            val amount = amountEditText.text.toString().toInt()

            setFragmentResult(
                "addCalcReqKey", bundleOf(
                    "calc_idBdlKey" to drinkId,
                    "calc_amountBdlKey" to amount,
                    "calc_drinkTimeBdlKey" to drinkTime
                )
            )

            dismiss()
        }

        view.findViewById<MaterialButton>(R.id.addCalcEntryBtnCancel).setOnClickListener {
            dismiss()
        }
    }

    companion object {
        const val TAG = "AddCalcEntryDialog"
    }
}
