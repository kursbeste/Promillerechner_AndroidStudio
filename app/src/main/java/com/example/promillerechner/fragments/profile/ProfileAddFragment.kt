package com.example.promillerechner.fragments.profile

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.R.id.autoCompleteTextView
import com.example.promillerechner.data.User
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.text.SimpleDateFormat
import java.util.*

class ProfileAddFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var mDisplayBirthdateLayout: TextInputLayout
    private var mDisplayBirthdate: EditText? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        mDisplayBirthdateLayout = view.findViewById(R.id.textInputLayoutBirthdate)
        mDisplayBirthdate = mDisplayBirthdateLayout.editText

        //Creates custom calendar with specific attributes matching for dates
        val customCalendar = Calendar.getInstance()
        val datePicker = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            customCalendar.set(Calendar.YEAR, year)
            customCalendar.set(Calendar.MONTH, month)
            customCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel(customCalendar)
        }

        //Opens calendar as datePicker
        mDisplayBirthdate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                datePicker,
                customCalendar.get(Calendar.YEAR) - 18,
                customCalendar.get(Calendar.MONTH),
                customCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        //Pulls genders from resources and makes them available as dropdown item
        val genders = resources.getStringArray(R.array.genders)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_gender, genders)
        val dropdown: AutoCompleteTextView = view.findViewById(autoCompleteTextView)
        dropdown.setAdapter(arrayAdapter)


        val addBtn: Button = view.findViewById(R.id.btnAddProfile)
        addBtn.setOnClickListener {
            insertDataToDatabase(view)
            //Navigates back, prevents backstacking
            findNavController().navigateUp()
        }

        val editTextName: TextInputEditText = view.findViewById(R.id.editTextName)
        val editTextBirthdate: TextInputEditText = view.findViewById(R.id.editTextBirthdate)

        //Checks if all fields are filled out
        addBtn.isEnabled = false
        val inputLayouts = listOf(editTextName, editTextBirthdate, dropdown)
        for (inputLayout in inputLayouts) {
            inputLayout.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    val et1 = editTextName.text.toString().trim()
                    val et2 = editTextBirthdate.text.toString().trim()
                    val ac = dropdown.text.toString().trim()

                    addBtn.isEnabled = et1.isNotEmpty()
                            && et2.isNotEmpty()
                            && ac.isNotEmpty()
                }

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                }
            })
        }
    }

    //Creates matching DateFormat for Birthdate
    private fun updateLabel(customCalendar: Calendar) {
        val customFormat = "dd.MM.yyyy"
        val sdf = SimpleDateFormat(customFormat, Locale.GERMANY)
        mDisplayBirthdate?.setText(sdf.format(customCalendar.time))
    }

    //Pulls all fields and creates new User object
    private fun insertDataToDatabase(view: View) {
        val nameEditText: EditText = view.findViewById(R.id.editTextName)
        val name = nameEditText.text.toString()

        val birthdateTextView: TextView = view.findViewById(R.id.editTextBirthdate)
        val birthdateString = birthdateTextView.text.toString()
        val birthdateSdf = SimpleDateFormat("dd.MM.yyyy")
        val birthdateDate: Date = birthdateSdf.parse(birthdateString)!!
        val birthdate = birthdateDate.time

        val genderACTV: AutoCompleteTextView = view.findViewById(autoCompleteTextView)
        val gender = genderACTV.text.toString().first()

        val heightSlider: Slider = view.findViewById(R.id.sliderHeight)
        val height: Int = heightSlider.value.toInt()

        val weightSlider: Slider = view.findViewById(R.id.sliderWeight)
        val weight: Int = weightSlider.value.toInt()

        val user = User(0, name, birthdate, gender, height, weight, true)
        viewModel.addUser(user)
        Toast.makeText(requireContext(), "Neues Profil hinzugefügt!", Toast.LENGTH_LONG).show()
    }
}