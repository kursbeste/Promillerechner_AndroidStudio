package com.example.promillerechner.fragments.drinks

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.example.promillerechner.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern


class DrinksAddDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drinks_add_dialog, container, false)
    }

    // Configure the Dialog to fill the whole width of the screen (with some margin)
    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val nameInput = view.findViewById<TextInputLayout>(R.id.addDrinkNameInputLayout)
        val alcVolInput = view.findViewById<TextInputLayout>(R.id.addDrinkAlcvolInputLayout)

        // Input-Filter for the alcohol-per-volume input
        // Only accepts decimal values between 0 and 100, while simultaneously only allowing one decimal place
        alcVolInput.editText?.filters = arrayOf(InputFilter { source, _, _, dest, dstart, dend ->
            val value = if (source.isEmpty()) {
                dest.removeRange(dstart, dend)
            } else {
                StringBuilder(dest).insert(dstart, source)
            }
            val matcher = Pattern.compile("""^[0-9][0-9]?([,.][0-9]?)?$""").matcher(value)
            if (!matcher.matches()) "" else null
        })

        val addBtn = view.findViewById<MaterialButton>(R.id.addDrinkBtnAccept)
        addBtn.isEnabled = false
        val editTexts = listOf(alcVolInput.editText, nameInput.editText)
        for (editText in editTexts) {
            editText?.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    val et1 = alcVolInput.editText?.text.toString().trim()
                    val et2 = nameInput.editText?.text.toString().trim()

                    addBtn.isEnabled = et1.isNotEmpty()
                            && et2.isNotEmpty()
                            && (et2.length <= resources.getInteger(R.integer.drink_name_max_chars))
                }

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                }
            })
        }

        addBtn.setOnClickListener {
            val drinkName = nameInput.editText?.text.toString()
            val alcVolString: String = alcVolInput.editText?.text.toString().replace(",", ".")
            val alcVolFloat = alcVolString.toFloatOrNull() as Float
            val alcVolInt: Int = (alcVolFloat * 10.0).toInt()

            setFragmentResult(
                "addDrinkReqKey", bundleOf(
                    "nameBdlKey" to drinkName,
                    "alcvolBdlKey" to alcVolInt
                )
            )

            dismiss()
        }

        view.findViewById<MaterialButton>(R.id.addDrinkBtnCancel).setOnClickListener {
            dismiss()
        }
    }

    companion object {
        const val TAG = "AddDrinkDialog"
    }
}
