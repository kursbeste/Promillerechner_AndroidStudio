package com.example.promillerechner.fragments.profile

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.User
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.lang.Long
import java.text.SimpleDateFormat
import java.util.*


class ProfileUpdateFragment : Fragment() {

    //User object gets passed by args
    private val args by navArgs<ProfileUpdateFragmentArgs>()
    private lateinit var viewModel: MainViewModel
    private lateinit var mDisplayBirthdateLayout: TextInputLayout
    private var mDisplayBirthdate: EditText? = null
    private val birthdateSdf = SimpleDateFormat("dd.MM.yyyy")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_update, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        mDisplayBirthdateLayout = view.findViewById(R.id.textInputUpdateBirthdate)
        mDisplayBirthdate = mDisplayBirthdateLayout.editText

        //Creates custom calendar with specific attributes matching for dates
        val customCalendar = Calendar.getInstance()
        val datePicker = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            customCalendar.set(Calendar.YEAR, year)
            customCalendar.set(Calendar.MONTH, month)
            customCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            mDisplayBirthdate?.setText(birthdateSdf.format(customCalendar.time))
        }

        //Opens calendar as datePicker
        mDisplayBirthdate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                datePicker,
                customCalendar.get(Calendar.YEAR) - 18,
                customCalendar.get(Calendar.MONTH),
                customCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        //Pulls genders from resources and makes them available as dropdown item
        val genders = resources.getStringArray(R.array.genders)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_gender, genders)
        val dropdown: AutoCompleteTextView = view.findViewById(R.id.autoCompleteTextViewUpdate)

        //Pulls gender from User object and pushes it into AutoCompleteTextView
        val selectedUserGender = if (args.selectedUser.gender == 'm') {
            resources.getStringArray(R.array.genders)[0]
        } else {
            resources.getStringArray(R.array.genders)[1]
        }
        dropdown.setText(selectedUserGender)
        dropdown.setAdapter(arrayAdapter)

        //Pulls birthdate from User object and parses it to StandardDateFormat
        val netDate = Date(Long.parseLong(args.selectedUser.birthdate.toString()))
        mDisplayBirthdate?.setText(birthdateSdf.format(netDate))

        //Pulls name, height and weight from User object and pushes values into its editText or spinner
        val editTextUpdateName: EditText = view.findViewById(R.id.editTextUpdateName)
        val sliderUpdateHeight: Slider = view.findViewById(R.id.sliderUpdateHeight)
        val sliderUpdateWeight: Slider = view.findViewById(R.id.sliderUpdateWeight)
        val editTextBirthdate: TextInputEditText = view.findViewById(R.id.editTextUpdateBirthdate)

        editTextUpdateName.setText(args.selectedUser.name)
        sliderUpdateHeight.value = args.selectedUser.height.toFloat()
        sliderUpdateWeight.value = args.selectedUser.weight.toFloat()


        val updateBtn: Button = view.findViewById(R.id.btnUpdateProfile)
        updateBtn.setOnClickListener {
            //Navigates back, prevents backstacking
            findNavController().navigateUp()
            updateData(view)
        }

        //Checks if all fields are filled out, default value = true since it is full at the start
        updateBtn.isEnabled = true
        val inputLayouts = listOf(editTextUpdateName, editTextBirthdate, dropdown)
        for (inputLayout in inputLayouts) {
            inputLayout.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    val et1 = editTextUpdateName.text.toString().trim()
                    val et2 = editTextBirthdate.text.toString().trim()
                    val ac = dropdown.text.toString().trim()

                    updateBtn.isEnabled = et1.isNotEmpty()
                            && et2.isNotEmpty()
                            && ac.isNotEmpty()
                }

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                }
            })
        }
        setHasOptionsMenu(true)
    }

    //Pulls all fields and updates User object with their input
    private fun updateData(view: View) {
        val nameUpdatedEditText: EditText = view.findViewById(R.id.editTextUpdateName)
        val name = nameUpdatedEditText.text.toString()

        //Birthdate needs casting to float for sql
        val birthdateUpdatedTextView: TextView = view.findViewById(R.id.editTextUpdateBirthdate)
        val birthdateString = birthdateUpdatedTextView.text.toString()
        val birthdateDate: Date = birthdateSdf.parse(birthdateString)
        val birthdate = birthdateDate.time

        //Gender needs to be casted as Char
        val genderUpdatedACTV: AutoCompleteTextView =
            view.findViewById(R.id.autoCompleteTextViewUpdate)
        val gender = genderUpdatedACTV.text.toString().first()

        val sliderUpdateHeight: Slider = view.findViewById(R.id.sliderUpdateHeight)
        val height: Int = sliderUpdateHeight.value.toInt()

        val sliderUpdateWeight: Slider = view.findViewById(R.id.sliderUpdateWeight)
        val weight: Int = sliderUpdateWeight.value.toInt()

        val updatedUser = User(args.selectedUser.id, name, birthdate, gender, height, weight, false)
        viewModel.updateUser(updatedUser)
        Toast.makeText(requireContext(), "Profil geupdated!", Toast.LENGTH_LONG).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete) {
            deleteUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteUser() {
        // Open delete dialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Ja") { _, _ ->
            viewModel.deleteUser(args.selectedUser)
            Toast.makeText(requireContext(), "Profil gelöscht!", Toast.LENGTH_LONG).show()
            // Navigate back to not stack up fragments
            findNavController().navigateUp()
        }
        builder.setNegativeButton("Nein") { _, _ -> }
        builder.setTitle("${args.selectedUser.name} löschen?")
        builder.create().show()
    }
}