package com.example.promillerechner.fragments.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.User
import java.lang.Long
import java.text.SimpleDateFormat
import java.util.*


class ProfileListAdapter(private val users: List<User>, private var viewModel: MainViewModel) :
    RecyclerView.Adapter<ProfileListAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.profileListItemTextViewName)
        val birthdateTextView: TextView = view.findViewById(R.id.profileListItemTextViewBirthdate)
        val profileListItem: ConstraintLayout = view.findViewById(R.id.profileListItem)
        val radioButton: RadioButton = view.findViewById(R.id.radioButtonSelected)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.profile_list_item, viewGroup, false)

        return ViewHolder(view)
    }

    //Users are saved as array and accessible via users[position]
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        //Checks if User is selected and activates radioButton
        viewHolder.radioButton.isChecked = users[position] == viewModel.getSelectedUser()

        //Pulls birthdate from User object and parses it to StandardDateFormat
        val birthdateSdf = SimpleDateFormat("dd.MM.yyyy")
        val netDate = Date(Long.parseLong(users[position].birthdate.toString()))

        viewHolder.nameTextView.text = users[position].name
        viewHolder.birthdateTextView.text = birthdateSdf.format(netDate)

        viewHolder.profileListItem.setOnClickListener {
            val action =
                ProfileListFragmentDirections.actionProfileListFragmentToProfileUpdateFragment(users[position])
            //Forwarding to ProfileUpdateFragment with User object as argument
            viewHolder.itemView.findNavController().navigate(action)
        }

        //Activates and Deactivates radioButton on Click and updates user_table
        viewHolder.radioButton.setOnClickListener {
            if (viewHolder.radioButton.isChecked) {
                val oldUser = viewModel.getSelectedUser()
                if (oldUser != null) {
                    oldUser.selected = false
                    viewModel.updateUser(oldUser)
                }
                val selectedUser = users[position]
                selectedUser.selected = true
                viewModel.updateUser(selectedUser)
            }
            //"Refreshes" Fragment to account for user_table entry
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = users.size
}
