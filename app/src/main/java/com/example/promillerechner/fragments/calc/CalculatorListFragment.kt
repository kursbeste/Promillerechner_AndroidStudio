package com.example.promillerechner.fragments.calc

import android.annotation.SuppressLint
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.CalculationEntry
import com.example.promillerechner.data.User
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.floor
import kotlin.math.roundToInt


class CalculatorListFragment : Fragment(), CalculatorListAdapter.AdapterCallback {

    class AlcTimeValue(val time: Float, val alcoholLevel: Float)

    class MyXAxisFormatter : ValueFormatter() {
        private val chartHours = arrayOf(
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"
        )

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return chartHours.getOrNull(value.toInt()) ?: value.toString()
        }
    }

    class MyValueFormatter : ValueFormatter() {
        private val format = DecimalFormat("0.00")

        // override this for e.g. LineChart or ScatterChart
        override fun getPointLabel(entry: Entry?): String {
            return format.format(entry?.y)
        }
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var calculatorListAdapter: CalculatorListAdapter
    private lateinit var chart: LineChart
    private lateinit var dropdown: AutoCompleteTextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calculator_list, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Deactivate layout and only activate if necessary conditions are met
        setLayoutActivated(false)

        // Init the connection to the database
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        // Instantiate all needed Inputs and Layouts
        dropdown = view.findViewById(R.id.chooseProfileAutoCompleteTextView)
        val recyclerView: RecyclerView = view.findViewById(R.id.calculatorListRecyclerView)
        val addEntryButton: Button = view.findViewById(R.id.calculatorAddEntryButton)
        chart = view.findViewById(R.id.chart) as LineChart

        // If no users in db, stop processing the whole fragment
        val usersList: List<User> = viewModel.getUsers()
        val profileArray = usersList.toTypedArray()
        if (profileArray.isEmpty()) {
            dropdown.isEnabled = false
            dropdown.isClickable = false
            return
        }

        // Retrieves profiles from db and formats them to be used in the dropdown
        val profileNames: Array<String?> = arrayOfNulls(profileArray.size)
        for (i in profileArray.indices) {
            profileNames[i] = profileArray[i].name
        }
        // Create the Adapter for the dropdown and populate it with the usernames
        val arrayAdapter = ArrayAdapter(
            requireContext(), R.layout.calculator_choose_profile_dropdown, profileNames
        )

        // Populate dropdown with standard-user, if available
        val selectedUser: User = viewModel.getSelectedUser()
        if (selectedUser != null) {
            dropdown.setText(selectedUser.name)

            // Only activate layout, if a preselected user could be put into the dropdown
            setLayoutActivated(true)
        }
        // Assign the adapter to the dropdown
        dropdown.setAdapter(arrayAdapter)

        // Init the RecyclerView Adapter with initial data from database
        val initialDataList = generateRecyclerViewEntries(usersList, dropdown.text.toString())
        calculatorListAdapter = CalculatorListAdapter(
            initialDataList,
            viewModel,
            this
        )

        // Only paint the graph at fragment start, if data is available
        if (initialDataList.size > 0) {
            var profile: User? = null
            for (i in profileArray.indices) {
                if (profileArray[i].name == dropdown.text.toString()) {
                    profile = profileArray[i]
                    break
                }
            }

            paintGraph(initialDataList, profile!!, chart)
        } else {
            (view.findViewById(R.id.soberTimeTextView) as TextView).visibility = INVISIBLE
            chart.setNoDataText("Noch keine Grafikdaten verfügbar.")
        }

        if (resources.configuration.orientation == ORIENTATION_LANDSCAPE) {
            val displayMetrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
            val width = displayMetrics.widthPixels

            chart.layoutParams.height = ConstraintLayout.LayoutParams.MATCH_PARENT
            chart.layoutParams.width = width / 2
            val scrollView: ScrollView = view.findViewById(R.id.scrollView2)
            scrollView.layoutParams.width = ConstraintLayout.LayoutParams.WRAP_CONTENT
            scrollView.isVerticalScrollBarEnabled = false

            dropdown.layoutParams.width = width / 2 - 50

            recyclerView.layoutParams.width = width / 2 - 50

            val constraintLayout: ConstraintLayout =
                view.findViewById(R.id.constraintLayoutCalcList)
            val constraintSet = ConstraintSet()
            constraintSet.clone(constraintLayout)

            constraintSet.clear(R.id.chart, ConstraintSet.START)
            constraintSet.clear(R.id.scrollView2, ConstraintSet.BOTTOM)

            val constraintLayout2: ConstraintLayout =
                view.findViewById(R.id.constraintLayoutCalcRecycler)
            val constraintSet2 = ConstraintSet()
            constraintSet2.clone(constraintLayout2)

            constraintSet2.clear(R.id.textInputLayoutProfile, ConstraintSet.LEFT)
            constraintSet2.clear(R.id.textInputLayoutProfile, ConstraintSet.RIGHT)
            constraintSet2.clear(R.id.textInputLayoutProfile, ConstraintSet.TOP)
            constraintSet2.clear(R.id.calculatorListRecyclerView, ConstraintSet.START)
            constraintSet2.clear(R.id.calculatorListRecyclerView, ConstraintSet.END)
            constraintSet2.connect(
                R.id.calculatorListRecyclerView,
                ConstraintSet.LEFT,
                R.id.scrollView2,
                ConstraintSet.LEFT
            )
            constraintSet2.connect(
                R.id.calculatorListRecyclerView,
                ConstraintSet.RIGHT,
                R.id.scrollView2,
                ConstraintSet.RIGHT
            )
            constraintSet2.applyTo(constraintLayout2)

            constraintSet.connect(
                R.id.scrollView2,
                ConstraintSet.LEFT,
                R.id.parent,
                ConstraintSet.LEFT
            )
            constraintSet.connect(
                R.id.scrollView2,
                ConstraintSet.BOTTOM,
                R.id.disclaimerTextView,
                ConstraintSet.TOP
            )
            constraintSet.connect(
                R.id.chart,
                ConstraintSet.LEFT,
                R.id.scrollView2,
                ConstraintSet.RIGHT
            )
            constraintSet.connect(R.id.chart, ConstraintSet.RIGHT, R.id.parent, ConstraintSet.RIGHT)
            constraintSet.connect(R.id.chart, ConstraintSet.TOP, R.id.parent, ConstraintSet.TOP)
            constraintSet.applyTo(constraintLayout)
        }

        // Add a listener to the dropdown to reload the fragment's main content on profile change
        dropdown.setOnItemClickListener { _, _, _, _ ->
            if (dropdown.text.isNotEmpty()) {
                // create list to populate the recyclerview and the graph
                val drinkDataList: ArrayList<CalculationEntry> = generateRecyclerViewEntries(
                    usersList,
                    dropdown.text.toString()
                )

                // Only paint the graph, if data is available
                if (drinkDataList.size > 0) {
                    var profile: User? = null
                    for (i in profileArray.indices) {
                        if (profileArray[i].name == dropdown.text.toString()) {
                            profile = profileArray[i]
                            break
                        }
                    }

                    paintGraph(drinkDataList, profile!!, chart)
                } else {
                    (view.findViewById(R.id.soberTimeTextView) as TextView).visibility = INVISIBLE
                    chart.setNoDataText("Noch keine Grafikdaten verfügbar.")
                    chart.invalidate()
                    chart.clear()
                }

                calculatorListAdapter.repopulateData(drinkDataList)
                setLayoutActivated(true)
            }
        }

        // Add dividers and the adapter to the recyclerview
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        recyclerView.adapter = calculatorListAdapter

        // Create a predefined LayoutManager and add to the recyclerview
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        // Listener, that triggers, when the add-Dialog was dismissed containing data to be added
        childFragmentManager.setFragmentResultListener(
            "addCalcReqKey",
            this
        ) { _, bundle ->
            // Retrieve data from the DialogFragment
            val drinkId = bundle.getInt("calc_idBdlKey")
            val drinkAmount = bundle.getInt("calc_amountBdlKey")
            val drinkTime = bundle.getInt("calc_drinkTimeBdlKey")

            // Traverse userlist to get the id of the current user
            for (user in usersList) {
                if (user.name == dropdown.text.toString()) {
                    // Format date
                    val dateInt: Int = getDateInt()

                    // Create a new entry-instance
                    val newEntry = CalculationEntry(
                        0,
                        user.id,
                        drinkId,
                        drinkAmount,
                        drinkTime,
                        dateInt
                    )
                    // Add entry to the database
                    viewModel.addEntry(newEntry)

                    // Get all entries from database, as we need the autogenerated ids and repopulate adapter
                    calculatorListAdapter.repopulateData(
                        ArrayList(
                            generateRecyclerViewEntries(
                                usersList,
                                dropdown.text.toString()
                            )
                        )
                    )

                    // Repaint the graph
                    paintGraph(calculatorListAdapter.entries, user, chart)

                    break
                }
            }
        }

        // Add Listener to add-Button and open DialogFragment if clicked
        addEntryButton.setOnClickListener {
            val dialog = CalcEntryAddDialogFragment()
            dialog.show(childFragmentManager, CalcEntryAddDialogFragment.TAG)
        }
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun deleteEntry() {
        if (dropdown.text.toString().isNotEmpty()) {
            for (user in viewModel.getUsers()) {
                // Only paint the graph, if data is available
                if (user.name == dropdown.text.toString()) {
                    if (calculatorListAdapter.entries.size > 0) {
                        paintGraph(calculatorListAdapter.entries, user, chart)
                    } else {
                        (view?.findViewById(R.id.soberTimeTextView) as TextView).visibility =
                            INVISIBLE
                        chart.setNoDataText("Noch keine Grafikdaten verfügbar.")
                        chart.invalidate()
                        chart.clear()
                    }
                    break
                }
            }
        }
    }

    // Function to calculate Alcohol Level Data based on the drinks List and the user parsed into the function
    private fun calculateAlcoholLevel(
        entries: ArrayList<CalculationEntry>, profile: User
    ): ArrayList<AlcTimeValue> {
        // set how much the Alcohol Level decreases per hour
        var alcoholLevelDecrease = 0.13F
        if (profile.gender == 'm') {
            alcoholLevelDecrease = 0.15F
        }
        // Calculate the reduction Factor based on gender, height and weight
        val reductionFactor: Float = when (profile.gender) {
            'm' -> {
                (1.055F * (2.447F + 0.1074F * profile.height.toFloat() + 0.3362F * profile.weight.toFloat())) / (0.8F * profile.weight.toFloat())
            }
            'w' -> {
                (1.055F * (-2.097F + 0.1069F * profile.height.toFloat() + 0.2466F * profile.weight.toFloat())) / (0.8F * profile.weight.toFloat())
            }
            else -> {
                // Failsafe:
                0.6F
            }
        }

        // Calculate the Alcohol Level Array that is returned at the end of the Function
        var currentAlcoholLevel = 0F
        val resultAlcoholLevel: ArrayList<AlcTimeValue> = arrayListOf()
        for (i in entries.indices) {
            currentAlcoholLevel +=
                (((viewModel.getDrinkById(entries[i].drinkId).alcvol * entries[i].amount / 10F) / 125F) / (profile.weight.toFloat() * reductionFactor))

            var drinkTime = getTimeInHours(entries[i].drinkTime)
            if (drinkTime < 8F) {
                drinkTime += 24F
            }
            resultAlcoholLevel.add(
                AlcTimeValue(
                    drinkTime,
                    (currentAlcoholLevel * 100).roundToInt().toFloat() / 100F
                )
            )
            // Calculate how much the AlcoholLevel decreases until the next drink
            if (i < entries.size - 1) {
                var lastDrinkTime: Float = getTimeInHours(entries[i].drinkTime)
                var nextDrinkTime = getTimeInHours(entries[i + 1].drinkTime)
                if (lastDrinkTime < 8F) {
                    lastDrinkTime += 24F
                }
                if (nextDrinkTime < 8F) {
                    nextDrinkTime += 24F
                }
                if (lastDrinkTime < nextDrinkTime) {
                    while (currentAlcoholLevel > 0F && lastDrinkTime + 1F <= nextDrinkTime) {
                        currentAlcoholLevel -= alcoholLevelDecrease
                        if (currentAlcoholLevel < 0F) {
                            currentAlcoholLevel += alcoholLevelDecrease
                            lastDrinkTime += currentAlcoholLevel / alcoholLevelDecrease
                            currentAlcoholLevel = 0F
                        } else {
                            lastDrinkTime += 1F
                        }
                        resultAlcoholLevel.add(
                            AlcTimeValue(
                                lastDrinkTime,
                                (currentAlcoholLevel * 100).roundToInt().toFloat() / 100F
                            )
                        )
                        if (currentAlcoholLevel == 0F) {
                            resultAlcoholLevel.add(
                                AlcTimeValue(
                                    nextDrinkTime,
                                    0F
                                )
                            )
                        }
                    }
                    if (currentAlcoholLevel > 0F && lastDrinkTime < nextDrinkTime) {
                        val timeBetweenDrinks = nextDrinkTime - lastDrinkTime
                        currentAlcoholLevel -= timeBetweenDrinks * alcoholLevelDecrease
                        if (currentAlcoholLevel < 0F) {
                            currentAlcoholLevel = 0F
                        }
                        resultAlcoholLevel.add(
                            AlcTimeValue(
                                nextDrinkTime,
                                (currentAlcoholLevel * 100).roundToInt().toFloat() / 100F
                            )
                        )
                    }
                }
            }
            // Calculate the Alcohol Level Decrease after the last drink
            else if (i == entries.size - 1) {
                var lastDrinkTime: Float = getTimeInHours(entries[entries.size - 1].drinkTime)
                if (lastDrinkTime < 8F) {
                    lastDrinkTime += 24F
                }
                while (currentAlcoholLevel > 0F) {
                    currentAlcoholLevel -= alcoholLevelDecrease
                    if (currentAlcoholLevel < 0F) {
                        currentAlcoholLevel += alcoholLevelDecrease
                        lastDrinkTime += currentAlcoholLevel / alcoholLevelDecrease
                        currentAlcoholLevel = 0F
                    } else {
                        lastDrinkTime += 1F
                    }
                    resultAlcoholLevel.add(
                        AlcTimeValue(
                            lastDrinkTime,
                            (currentAlcoholLevel * 100).roundToInt().toFloat() / 100F
                        )
                    )
                }
            }
        }
        // Print time when user is sober
        if (resultAlcoholLevel.size > 0) {
            val soberTimeTextView: TextView = view!!.findViewById(R.id.soberTimeTextView)
            var soberHour: Int =
                floor(resultAlcoholLevel[resultAlcoholLevel.size - 1].time.toDouble()).toInt()
            val soberMinute: Int =
                ((resultAlcoholLevel[resultAlcoholLevel.size - 1].time % 1F) * 60F).toInt()
            while (soberHour >= 24) {
                soberHour -= 24
            }
            soberTimeTextView.visibility = VISIBLE
            soberTimeTextView.text = "Wieder nüchtern um: " + soberHour.toString()
                .padStart(2, '0') + ":" + soberMinute.toString().padStart(2, '0') + " Uhr"
        }
        return resultAlcoholLevel
    }

    // Function that formats Time from "1820" to "18,33"
    private fun getTimeInHours(time: Int): Float {
        return (time / 100).toFloat() + time.toFloat() % 100F / 60F
    }

    // Retrieve the current date and format it appropriately
    private fun getDateInt(): Int {
        val customCalendar = Calendar.getInstance()
        val year = customCalendar.get(Calendar.YEAR)
        val month = customCalendar.get(Calendar.MONTH) + 1
        val day = customCalendar.get(Calendar.DAY_OF_MONTH)

        return if (customCalendar.get(Calendar.HOUR_OF_DAY) < 8) {
            year * 10000 + month * 100 + day - 1
        } else {
            year * 10000 + month * 100 + day
        }
    }

    // Generates a sorted list of CalculationEntries
    // Retrieves data from the database, if available, otherwise returns empty list
    private fun generateRecyclerViewEntries(
        usersList: List<User>,
        input: String
    ): ArrayList<CalculationEntry> {
        var entries: ArrayList<CalculationEntry> = ArrayList<CalculationEntry>()
        for (user in usersList) {
            if (user.name == input) {
                entries = ArrayList(viewModel.getEntriesByDayAndUser(getDateInt(), user.id))
                entries.sortWith { o1, o2 ->
                    var time1 = o1.drinkTime
                    var time2 = o2.drinkTime
                    if (time1 in 0..799) {
                        time1 += 2400
                    }
                    if (time2 in 0..799) {
                        time2 += 2400
                    }
                    time1.compareTo(time2)
                }
                break
            }
        }
        return entries
    }

    // Repaints the LineChart
    @RequiresApi(Build.VERSION_CODES.M)
    private fun paintGraph(
        drinkDataList: ArrayList<CalculationEntry>,
        profile: User,
        chart: LineChart
    ) {
        val dataObjects: ArrayList<AlcTimeValue> =
            calculateAlcoholLevel(drinkDataList, profile)
        val entries: ArrayList<Entry> = ArrayList()
        for (data in dataObjects) {
            entries.add(Entry(data.time, data.alcoholLevel))
        }

        val xAxis = chart.xAxis
        val leftYAxis = chart.axisLeft
        val rightYAxis = chart.axisRight
        val dataSet = LineDataSet(entries, "Promillewert")
        dataSet.valueFormatter = MyValueFormatter()
        val lineData = LineData(dataSet)

        chart.xAxis.valueFormatter = MyXAxisFormatter()
        chart.xAxis.axisMinimum = 8F
        chart.xAxis.axisMaximum = 32F
        chart.data = lineData
        chart.description.isEnabled = false
        xAxis.textColor = ContextCompat.getColor(requireContext(), R.color.md_list_item_textcolor)
        leftYAxis.textColor =
            ContextCompat.getColor(requireContext(), R.color.md_list_item_textcolor)
        rightYAxis.textColor =
            ContextCompat.getColor(requireContext(), R.color.md_list_item_textcolor)
        dataSet.valueTextColor =
            ContextCompat.getColor(requireContext(), R.color.md_list_item_textcolor)
        chart.legend.isEnabled = false
        chart.invalidate()
        dataSet.valueTextSize = 10F
    }

    private fun setLayoutActivated(activated: Boolean) {
        val noticeTextView: TextView = view!!.findViewById(R.id.calculatorListNoticeTextView)
        val drinksTextView: TextView = view!!.findViewById(R.id.calculatorListDrinksTextView)
        val recyclerView: RecyclerView = view!!.findViewById(R.id.calculatorListRecyclerView)
        val addEntryButton: Button = view!!.findViewById(R.id.calculatorAddEntryButton)
        val chart = view!!.findViewById(R.id.chart) as LineChart

        if (activated) {
            noticeTextView.visibility = INVISIBLE
            drinksTextView.visibility = VISIBLE
            recyclerView.visibility = VISIBLE
            addEntryButton.visibility = VISIBLE
            chart.visibility = VISIBLE
        } else {
            noticeTextView.visibility = VISIBLE
            drinksTextView.visibility = INVISIBLE
            recyclerView.visibility = INVISIBLE
            addEntryButton.visibility = INVISIBLE
            chart.visibility = INVISIBLE
        }
    }
}
