package com.example.promillerechner.fragments.drinks

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainActivity
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.Drink
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DrinksListFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var listAdapter: DrinksListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drinks_list, container, false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init the connection to the database
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        // Init the RecyclerView-Adapter with the data list
        val drinks: ArrayList<Drink> = ArrayList(viewModel.getAlphabetizedDrinks())
        val main = activity as MainActivity
        listAdapter = DrinksListAdapter(drinks, main)

        // Get a reference to the RecyclerView and add the adapter
        val recyclerView: RecyclerView = view.findViewById(R.id.drinksListRecyclerView)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        recyclerView.adapter = listAdapter

        // Create a predefined LayoutManager and add to the recyclerview
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        // Get a reference to the SearchView and configure it to be shown permanently
        val searchBar: SearchView = view.findViewById(R.id.drinksListSearchBar)
        searchBar.setIconifiedByDefault(false)

        // Add a listener to the SearchView and notify the Adapter about changes
        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                listAdapter.getFilter().filter(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                listAdapter.getFilter().filter(newText)
                return true
            }
        })

        // Add a listener to the Dialog Child-Fragment to catch the newly added drink data
        childFragmentManager.setFragmentResultListener(
            "addDrinkReqKey",
            this
        ) { _, bundle ->
            // Retreive the name and the alcohol by volume
            val drinkName = bundle.getString("nameBdlKey") as String
            val drinkAlcvol = bundle.getInt("alcvolBdlKey")

            // Create a new Drink-object, write it to the database and retreive the returned rowId
            val newDrink = Drink(0, drinkName, drinkAlcvol)
            val rowId: Long = viewModel.addDrink(newDrink)
            // Get drinkId from rowId
            val drinkId: Int = viewModel.getDrinkIdFromRowId(rowId)
            // Change the drinkId of the Drink-object and add it to the adapter's data
            newDrink.id = drinkId
            listAdapter.addDrink(newDrink)
        }

        // Get a reference to the FloatingActionButton and add a listener to it, opening the DialogFragment
        val addDrinkBtn: FloatingActionButton = view.findViewById(R.id.floatActBtnAddDrink)
        addDrinkBtn.setOnClickListener {
            val dialog = DrinksAddDialogFragment()
            dialog.show(childFragmentManager, DrinksAddDialogFragment.TAG)
        }
    }
}
