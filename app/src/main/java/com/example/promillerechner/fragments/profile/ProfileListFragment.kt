package com.example.promillerechner.fragments.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.User
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ProfileListFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        val users: List<User> = viewModel.getUsers()

        if(users.isEmpty())
        {
            val defaultUserTextView: TextView = view.findViewById(R.id.textViewDescription)
            defaultUserTextView.text = ""
        }

        //RecyclerView is created as List of Users
        val usersAdapter = ProfileListAdapter(users, viewModel)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val recyclerView: RecyclerView = view.findViewById(R.id.profileListRecyclerView)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = usersAdapter

        val addBtn: FloatingActionButton = view.findViewById(R.id.floatActBtnAddProfile)
        addBtn.setOnClickListener {
            //Navigates to ProfileAddFragment by following nav_graph
            findNavController().navigate(ProfileListFragmentDirections.actionProfileListFragmentToProfileAddFragment())
        }
    }
}