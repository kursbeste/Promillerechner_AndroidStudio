package com.example.promillerechner.fragments.drinks

import android.view.*
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.promillerechner.MainActivity
import com.example.promillerechner.MainViewModel
import com.example.promillerechner.R
import com.example.promillerechner.data.Drink
import com.google.android.material.checkbox.MaterialCheckBox
import java.util.*
import kotlin.collections.ArrayList

class DrinksListAdapter(
    drinksInput: ArrayList<Drink>,
    private val activity: MainActivity
) :
    RecyclerView.Adapter<DrinksListAdapter.ViewHolder>() {

    class DrinkPOJO(val drink: Drink) {
        var selected: Boolean = false
    }

    private var viewModel: MainViewModel

    // Create copy of drinks-ArrayList to keep an unaltered copy of thew data for searching
    var initialDrinks = ArrayList<DrinkPOJO>()
    var isDeleteMode = false
    var selectedAmount: MutableLiveData<Int> = MutableLiveData<Int>()
    val drinks: ArrayList<DrinkPOJO> = ArrayList<DrinkPOJO>()

    init {
        for (drinkInput in drinksInput) {
            drinks.add(DrinkPOJO(drinkInput))
        }
        initialDrinks = ArrayList<DrinkPOJO>(drinks)

        selectedAmount.value = 0

        viewModel = ViewModelProvider(activity).get(MainViewModel::class.java)
    }

    fun addDrink(drink: Drink) {
        drinks.add(DrinkPOJO(drink))
        drinks.sortWith { o1, o2 -> o1.drink.name.compareTo(o2.drink.name) }
        initialDrinks.add(DrinkPOJO(drink))
        initialDrinks.sortWith { o1, o2 -> o1.drink.name.compareTo(o2.drink.name) }
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.drinksListItemIcon)
        val nameTextView: TextView = view.findViewById(R.id.drinksListItemName)
        val alcvolTextView: TextView = view.findViewById(R.id.drinksListItemAlcvol)
        val itemCheckbox: MaterialCheckBox = view.findViewById(R.id.drinksListItemCheckbox)
    }

    // Function, that creates new ViewHolders as needes by the view
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // each ViewHolder gets the generic list item layout, but no specific data
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.drinks_list_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Function, that binds a ViewHolder to a specific data element
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Fill the empty item layouts with actual data
        val alcVol: Int = drinks[viewHolder.bindingAdapterPosition].drink.alcvol

        when {
            alcVol < 26 -> viewHolder.imageView.setImageResource(R.drawable.ic_round_no_drinks_24)
            alcVol < 65 -> viewHolder.imageView.setImageResource(R.drawable.ic_round_sports_bar_24)
            alcVol < 150 -> viewHolder.imageView.setImageResource(R.drawable.ic_round_wine_bar_24)
            alcVol < 280 -> viewHolder.imageView.setImageResource(R.drawable.ic_round_local_bar_24)
            else -> viewHolder.imageView.setImageResource(R.drawable.ic_round_liquor_24)
        }

        viewHolder.nameTextView.text = drinks[viewHolder.bindingAdapterPosition].drink.name
        val alcvolString: String = (alcVol.toFloat() / 10).toString() + "%"
        viewHolder.alcvolTextView.text = alcvolString

        if (isDeleteMode) {
            viewHolder.itemCheckbox.visibility = View.VISIBLE
            viewHolder.itemCheckbox.isChecked = drinks[viewHolder.bindingAdapterPosition].selected

            // Add a listener to the view to alter the datalists of the adapter
            viewHolder.itemView.setOnClickListener {
                viewHolder.itemCheckbox.toggle()
                if (viewHolder.itemCheckbox.isChecked) {
                    drinks[viewHolder.bindingAdapterPosition].selected = true
                    initialDrinks[viewHolder.bindingAdapterPosition].selected = true
                    selectedAmount.value = selectedAmount.value?.plus(1)
                } else {
                    drinks[viewHolder.bindingAdapterPosition].selected = false
                    initialDrinks[viewHolder.bindingAdapterPosition].selected = false
                    selectedAmount.value = selectedAmount.value?.minus(1)
                }
            }

            viewHolder.itemCheckbox.setOnClickListener {
                if (viewHolder.itemCheckbox.isChecked) {
                    drinks[viewHolder.bindingAdapterPosition].selected = true
                    initialDrinks[viewHolder.bindingAdapterPosition].selected = true
                    selectedAmount.value = selectedAmount.value?.plus(1)
                } else {
                    drinks[viewHolder.bindingAdapterPosition].selected = false
                    initialDrinks[viewHolder.bindingAdapterPosition].selected = false
                    selectedAmount.value = selectedAmount.value?.minus(1)
                }
            }
        } else {
            viewHolder.itemCheckbox.visibility = View.INVISIBLE

            viewHolder.itemView.setOnLongClickListener {
                selectedAmount.value = selectedAmount.value?.plus(1)

                val callback: ActionMode.Callback = object : ActionMode.Callback {

                    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                        // Inflate a menu resource providing context menu items
                        val inflater: MenuInflater = mode.menuInflater
                        inflater.inflate(R.menu.appbar, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode,
                    //      but may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                        isDeleteMode = true
                        drinks[viewHolder.bindingAdapterPosition].selected = true
                        initialDrinks[viewHolder.bindingAdapterPosition].selected = true
                        notifyItemRangeChanged(0, itemCount)

                        // Add an observer to the counting variable of selected drinks
                        selectedAmount.observe(activity, {
                            // Adjust the AppBar title accordingly
                            mode.title = "${selectedAmount.value} ausgewählt"
                        })

                        return true
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                        return when (item.itemId) {
                            R.id.action_delete -> {
                                // Note: Reverse-traversing the list while deleting to not run into an IndexOutOfBounds
                                for (i in drinks.size - 1 downTo 0) {
                                    if (drinks[i].selected) {
                                        drinks.removeAt(i)
                                    }
                                }
                                for (i in initialDrinks.size - 1 downTo 0) {
                                    if (initialDrinks[i].selected) {
                                        viewModel.removeDrink(initialDrinks[i].drink.id)
                                        initialDrinks.removeAt(i)
                                    }
                                }
                                mode.finish()
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode) {
                        // Remove observer from LiveData to avoid memory leakage
                        selectedAmount.removeObservers(activity)

                        isDeleteMode = false
                        for (drink in drinks) {
                            drink.selected = false
                        }
                        for (drink in initialDrinks) {
                            drink.selected = false
                        }
                        selectedAmount.value = 0
                        notifyDataSetChanged()
                    }
                }

                activity.startActionMode(callback)
                true
            }
        }
    }

    // Function, that returns the dataset's size
    override fun getItemCount() = drinks.size


    fun getFilter(): Filter {
        return searchFilter
    }

    private val searchFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList: ArrayList<DrinkPOJO> = ArrayList()
            if (constraint == null || constraint.isEmpty()) {
                initialDrinks.let { filteredList.addAll(it) }
            } else {
                val query = constraint.toString().trim().toLowerCase(Locale.ROOT)
                initialDrinks.forEach {
                    if (it.drink.name.toLowerCase(Locale.ROOT).contains(query)) {
                        filteredList.add(it)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if (results?.values is ArrayList<*>) {
                drinks.clear()
                drinks.addAll(results.values as ArrayList<DrinkPOJO>)
                notifyDataSetChanged()
            }
        }
    }
}
