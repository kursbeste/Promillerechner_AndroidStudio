package com.example.promillerechner.fragments.calc

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.example.promillerechner.R
import com.example.promillerechner.data.Drink
import java.util.*
import kotlin.collections.ArrayList


class CalcAddDialogDropdownAdapter(context: Context, inputList: List<Drink>) :
    ArrayAdapter<Drink?>(context, 0, inputList) {

    var initialDrinks: ArrayList<Drink> = ArrayList(inputList)

    override fun getFilter(): Filter {
        return drinkFilter
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val mConvertView: View = convertView
            ?: LayoutInflater.from(context).inflate(
                R.layout.calculator_add_entry_dropdown, parent, false
            )

        val textViewName: TextView = mConvertView.findViewById(R.id.addEntryDropdownItemName)
        val textViewAlcvol: TextView = mConvertView.findViewById(R.id.addEntryDropdownItemAlcvol)

        val drink: Drink? = getItem(position)
        if (drink != null) {
            textViewName.text = drink.name
            textViewAlcvol.text = (drink.alcvol.toFloat() / 10).toString() + "%"
        }
        return mConvertView
    }

    private val drinkFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val suggestions: ArrayList<Drink> = ArrayList()
            if (constraint == null || constraint.isEmpty()) {
                suggestions.addAll(initialDrinks)
            } else {
                val filterPattern =
                    constraint.toString().toLowerCase(Locale.ROOT).trim { it <= ' ' }
                for (item in initialDrinks) {
                    if (item.name.toLowerCase(Locale.ROOT).contains(filterPattern)) {
                        suggestions.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = suggestions
            results.count = suggestions.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            clear()
            addAll(results.values as List<Drink>)
            notifyDataSetChanged()
        }

        override fun convertResultToString(resultValue: Any): CharSequence {
            return (resultValue as Drink).name
        }
    }
}