package com.example.promillerechner.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "drinks_table")
data class Drink(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    val name: String,
    val alcvol: Int,         // Alcohol by volume; value between 0 and 1000; 37.5% = 375
)