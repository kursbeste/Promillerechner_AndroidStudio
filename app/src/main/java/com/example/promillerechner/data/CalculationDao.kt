package com.example.promillerechner.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CalculationDao {
    @Insert
    suspend fun addEntry(entry: CalculationEntry)

    @Insert
    suspend fun addEntries(entries: List<CalculationEntry>)

    @Query("DELETE FROM calc_table WHERE id = :id")
    suspend fun deleteEntryById(id: Int)

    @Query("DELETE FROM calc_table WHERE userId = :userId")
    suspend fun deleteEntriesOfUser(userId: Int)

    @Query("SELECT * FROM calc_table WHERE id = :id")
    suspend fun getEntryById(id: Int): CalculationEntry

    @Query("SELECT * FROM calc_table WHERE drinkDate = :date AND userId = :userId")
    suspend fun getEntriesByDayAndUser(date: Int, userId: Int): List<CalculationEntry>
}