package com.example.promillerechner.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface DrinkDao {

    @Insert
    suspend fun addDrink(drink: Drink): Long

    @Insert
    suspend fun addDrinks(drinks: List<Drink>)

    @Query("DELETE FROM drinks_table WHERE id = :id")
    suspend fun deleteDrinkById(id: Int)

    @Query("SELECT * FROM drinks_table ORDER BY name ASC")
    suspend fun getAlphabetizedDrinks(): List<Drink>

    @Query("SELECT * FROM drinks_table WHERE id = :id")
    suspend fun getDrinkById(id: Int): Drink

    @Query("SELECT id FROM drinks_table WHERE rowid = :rowId")
    suspend fun getDrinkIdFromRowId(rowId: Long): Int
}
