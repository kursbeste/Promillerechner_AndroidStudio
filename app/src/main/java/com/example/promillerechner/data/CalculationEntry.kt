package com.example.promillerechner.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "calc_table")
class CalculationEntry(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val userId: Int,
    val drinkId: Int,
    val amount: Int,
    val drinkTime: Int,     // Int with four digits: hhmm
    val drinkDate: Int,     // Int with eight digits: yyyymmdd
)
