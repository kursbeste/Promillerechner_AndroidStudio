package com.example.promillerechner.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_table")
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name: String,
    val birthdate: Long,
    val gender: Char,
    var height: Int,        // in cm
    var weight: Int,        // in kg
    var selected: Boolean,
) : Parcelable