package com.example.promillerechner.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


@Database(entities = [User::class, Drink::class, CalculationEntry::class], version = 1)
abstract class CustomDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun drinkDao(): DrinkDao
    abstract fun calculationDao(): CalculationDao

    companion object {
        @Volatile
        private var INSTANCE: CustomDatabase? = null

        fun getInstance(context: Context): CustomDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: getDatabase(context).also { INSTANCE = it }
            }

        fun getDatabase(context: Context): CustomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CustomDatabase::class.java,
                    "users_drinks_database"
                )
                    .allowMainThreadQueries()
                    .addCallback(object : Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            // insert the data on the IO Thread
                            MainScope().launch(Dispatchers.IO) {
                                getInstance(context).drinkDao().addDrinks(populateData())
                            }
                        }
                    })
                    .build()

                INSTANCE = instance

                // return instance
                instance
            }
        }

        private fun populateData(): List<Drink> {
            return listOf(
                Drink(0, "Bier", 52),
                Drink(0, "Rotwein", 138),
                Drink(0, "Weißwein", 120),
                Drink(0, "Rosé", 111),
                Drink(0, "Federweißer", 48),
                Drink(0, "Sekt", 113),
                Drink(0, "Champagner", 120),
                Drink(0, "Skyy Vodka", 400),
                Drink(0, "Absolut Vodka", 400),
                Drink(0, "Belvedere Vodka", 400),
                Drink(0, "Three Sixty Vodka", 375),
                Drink(0, "Smirnoff Vodka", 375),
                Drink(0, "Grey Goose Vodka", 400),
                Drink(0, "Puschkin Vodka", 375),
                Drink(0, "Russian Standard Vodka", 400),
                Drink(0, "Jelzin Vodka", 375),
                Drink(0, "Bacardi Carta Blanca", 375),
                Drink(0, "Bacardi Oakheart", 350),
                Drink(0, "Bacardi Razz", 350),
                Drink(0, "Captain Morgan", 350),
                Drink(0, "Captain Morgan White", 375),
                Drink(0, "Jägermeister", 350),
                Drink(0, "Jagdfürst", 350),
                Drink(0, "Mümmelmann", 350),
                Drink(0, "Highland Park", 400),
                Drink(0, "Johnnie Walker RedLabel", 400),
                Drink(0, "Johnnie Walker BlackLabel", 400),
                Drink(0, "Jack Daniels", 400),
                Drink(0, "Jim Beam", 400),
                Drink(0, "Seven Oaks", 400),
                Drink(0, "Western Gold", 400),
                Drink(0, "Bombay Sapphire", 400),
                Drink(0, "Tanqueray", 400),
                Drink(0, "Pfeffi", 180),
                Drink(0, "Berliner Luft", 180),
                Drink(0, "Klopfer", 160),
                Drink(0, "Berentzen Wildkirsche", 160),
                Drink(0, "Berentzen Saurer Apfel", 180),
                Drink(0, "Berentzen Maracuja", 180),
                Drink(0, "Berentzen Apfel", 180),
                Drink(0, "Berentzen Waldmeister", 150),
                Drink(0, "Berentzen Waldfrucht", 160),
                Drink(0, "Absinth", 800),
                Drink(0, "Sierra Tequila Silver", 380),
                Drink(0, "Sierra Tequila Reposado", 380),
                Drink(0, "Malibu", 210),
                Drink(0, "Batida de Coco", 160),
                Drink(0, "Erdbeerlimes", 150),
                Drink(0, "Apfelwein", 68),
                Drink(0, "Bembel weiß", 40),
                Drink(0, "Bembel grün", 40),
                Drink(0, "Bembel schwarz", 60),
                Drink(0, "Bembel rot", 42),
                Drink(0, "Asbach", 380),
                Drink(0, "Ouzo", 400),
                Drink(0, "Caipirinha", 300),
                Drink(0, "Mojito", 200),
                Drink(0, "Tequila Sunrise", 150),
                Drink(0, "Long Island Iced Tea", 150),
                Drink(0, "Cosmopolitan", 150),
                Drink(0, "Pina Colada", 150),
                Drink(0, "Cuba Libre", 150),
                Drink(0, "Margarita", 150),
                Drink(0, "Daiquiri", 150),
                Drink(0, "Sex on the Beach", 150),
                Drink(0, "Swimming Pool", 150),
                Drink(0, "Zombie Cocktail", 150),
                Drink(0, "White Russian", 150),
                Drink(0, "Bloody Mary", 150),
                Drink(0, "Skinny Bitch", 150)
            )
        }
    }
}
